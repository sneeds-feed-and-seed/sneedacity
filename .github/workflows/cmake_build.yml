name: CMake Build

on:
  push:
  pull_request:

defaults:
  run:
    shell: bash

jobs:
  build:
    name: ${{ matrix.config.name }}
    runs-on: ${{ matrix.config.os }}
    env:
      SNEEDACITY_CMAKE_GENERATOR: ${{ matrix.config.generator }}
      SNEEDACITY_ARCH_LABEL: ${{ matrix.config.arch }}
      # Windows codesigning
      # This variables will be used by all the steps
      WINDOWS_CERTIFICATE: ${{ secrets.WINDOWS_CERTIFICATE }}
      WINDOWS_CERTIFICATE_PASSWORD: ${{ secrets.WINDOWS_CERTIFICATE_PASSWORD }}
    strategy:
      fail-fast: false
      matrix:
        config:

        - name: Ubuntu_18.04
          os: ubuntu-18.04
          arch: x86_64 # as reported by `arch` or `uname -m`
          generator: Ninja

        - name: macOS_Intel
          os: macos-latest
          arch: Intel # as reported by Apple menu > About This Mac
          generator: Ninja

        - name: Windows_32bit
          os: windows-latest
          arch: x86 # as reported by Windows Settings > System > About
          generator: Ninja
          cc: cl
          cxx: cl

        - name: Windows_64bit
          os: windows-latest
          arch: amd64 # as reported by Windows Settings > System > About
          generator: Ninja
          cc: cl
          cxx: cl

    steps:
    - name: Checkout
      uses: actions/checkout@v2

    - uses: seanmiddleditch/gha-setup-ninja@master
    
    
    - name: Dependencies
      run: |
        exec bash "scripts/ci/dependencies.sh"

    - name: Environment
      run: |
        source "scripts/ci/environment.sh"

    - name: "Set up MSVC Developer Command Prompt"
      if: startswith( matrix.config.os, 'windows' )
      uses: seanmiddleditch/gha-setup-vsdevenv@v3
      with:
        arch: ${{ matrix.config.arch }}

    
    #- name: Install Apple codesigning certificates
    #  uses: apple-actions/import-codesign-certs@v1
    #  if: startswith( matrix.config.os, 'macos' ) && github.event_name == 'push' && github.repository_owner == 'audacity'
    #  with: 
    #    p12-file-base64: ${{ secrets.APPLE_CERTIFICATE }}
    #    p12-password: ${{ secrets.APPLE_CERTIFICATE_PASSWORD }}

    - name: Configure
      env:
        # Error reporing
        SENTRY_DSN_KEY: ${{ secrets.SENTRY_DSN_KEY }}
        SENTRY_HOST: ${{ secrets.SENTRY_HOST }}
        SENTRY_PROJECT: ${{ secrets.SENTRY_PROJECT }}
        # Apple code signing
        APPLE_CODESIGN_IDENTITY: ${{ secrets.APPLE_CODESIGN_IDENTITY }}
        APPLE_NOTARIZATION_USER_NAME: ${{ secrets.APPLE_NOTARIZATION_USER_NAME }}
        APPLE_NOTARIZATION_PASSWORD: ${{ secrets.APPLE_NOTARIZATION_PASSWORD }}
        CC: ${{ matrix.config.cc }}
        CXX: ${{ matrix.config.cxx }}
      run: |
        exec bash "scripts/ci/configure.sh"

    - name: Build
      run: |
        exec bash "scripts/ci/build.sh"

    - name: Install
      run: |
        exec bash "scripts/ci/install.sh"

    - name: Package
      run: |
        exec bash "scripts/ci/package.sh"

    - name: Upload artifact
      uses: actions/upload-artifact@v2
      with:
        name: Sneedacity_${{ matrix.config.name }}_${{ github.run_id }}_${{ env.GIT_HASH_SHORT }}
        path: |
          build/package/*
          !build/package/_CPack_Packages
        if-no-files-found: error
